const { validationResult } = require('express-validator');
const { errResponse } = require('../middleware/HandleError/HandleError');
const { generateToken, verifyToken, decodedToken } = require('../middleware/Auth/Auth');

const obtenerToken = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
            //data: err.dataErr  //descomentar si se requiere explciacion por cada campo (validator)
        });
    }
    //console.log(req.body);
        const dataToken = req.body;
        const newtoken = await generateToken(dataToken);
        let dataResponse = {
            token: newtoken
        }
        return res.status(200).send({ status: 200, message: 'EXITO', data: dataResponse });
}

module.exports = {
    obtenerToken
}