const crypto = require("crypto");

const { validationResult } = require('express-validator');
const { errResponse } = require('../middleware/HandleError/HandleError');

const {
    usuarios, sequelize,
} = require('../db');
const { resourceUsage } = require("process");

//Login USER
const loginUser = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
            //data: err.dataErr  //descomentar si se requiere explciacion por cada campo (validator)
        });
    }
    const tran= await sequelize.transaction();
    try {
        let { email, password } = req.body;

        let passwordMd5 = await encodeMd5(password);

        let cuentaUsuario = await usuarios.findOne({
            where: {
                email: email,
                password: passwordMd5
            },
            attributes: { exclude: ['email','password','activo'] },
            raw: true,
            transaction: tran
        });

        if(cuentaUsuario) {
            await tran.commit();
            return res.status(200).send({status: 200,message: 'Usuario encontrado.', data: cuentaUsuario});
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'Favor de verificar los datos ingresados.', data: {} });
        }

    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al intentar Ingresar.', data: error.toString()});
    }
}

//Alta
const createUser = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
            //data: err.dataErr  //descomentar si se requiere explciacion por cada campo (validator)
        });
    }
    const tran= await sequelize.transaction();
    try {
        
        let { nombre, paterno, materno, email, password } = req.body;
        
        let bStrongPass = await validarContraseña(password);

        if(bStrongPass == true) {
            let passwordMd5 = await encodeMd5(password);
            
            let [usuario, created] = await usuarios.findOrCreate({
                where: {
                    email: email
                },
                defaults: {
                    nombre: nombre,
                    apPaterno: paterno,
                    apMaterno: materno,
                    email: email,
                    password: passwordMd5
                },
                raw: true,
                transaction: tran
            });

            if (created) {
                delete usuario.dataValues.password;//Se elimina contraseña por ser un dato sensible
                await tran.commit();
                return res.status(201).send({status: 201,message: 'Se ha registrado con exito', data: usuario});
            } else {
                await tran.rollback();
                usuario = {};
                return res.status(409).send({status: 409,message: 'Este correo ya esta en registrado',data: usuario});
            }
        } else {
            await tran.rollback();
            return res.status(409).send({status: 409,message: bStrongPass,data: {}});
        }
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al crear usuario', data: error.toString()});
    }
   
}

//Actualización
const updateUser = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
            //data: err.dataErr  //descomentar si se requiere explciacion por cada campo (validator)
        });
    }
    const tran= await sequelize.transaction();
    
    try{
        let uuid = req.params.id;
        let usuario = await usuarios.findOne({
            where: {
                uuid: uuid,
                activo: 1
            },
            attributes: { exclude: ['activo','uuid','password','id','fecha_alta'] },
            raw: true,
            transaction: tran
        });
        if(usuario) {
            let { password } = req.body;
            let [passwordMd5] = await encodeMd5(password);
            let cambios = {
                password:passwordMd5
            }
            await usuarios.update(cambios,{
                returning: true,
                where: { uuid: uuid },
                transaction: tran
            });
            
            await tran.commit();
            return res.status(200).send({ status: 200, message: 'La actualización se ha realizado con éxito.', data: usuario });
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'No existe el usuario.', data: {} });
        }
    }catch(error){
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al actualizar usuario', data: error.toString()});
    }
}

//Baja Usuario
const disableUser = async (req, res) => {
    const tran= await sequelize.transaction();
    try{
        let uuid = req.params.id;
        let usuario = await usuarios.findOne({
            where: {
                uuid: uuid,
                activo: 1
            },
            raw: true,
            transaction: tran
        })
        if(usuario) {
            let cambios = {
                activo: 0
            }
            await usuarios.update(cambios,{
                returning: true,
                where: { uuid: uuid },
                transaction: tran
            });
            
            await tran.commit();
            return res.status(200).send({ status: 200, message: 'El usuario se ha dado de baja con éxito.', data: {} });
        } else {
            return res.status(404).send({ status: 404, message: 'No existe el usuario o ya se encuentra dado de baja.', data: {} });
        }
    }catch(error){
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al dar de baja al usuario.', data: error.toString()});
    }
}

//Eliminar Usuario
const deleteUser = async (req, res) => {
    const tran= await sequelize.transaction();
    try{
        let uuid = req.params.id;
        let usuario = await usuarios.findOne({
            where: {
                uuid: uuid
            },
            raw: true,
            transaction: tran
        })
        if(usuario) {
            await usuarios.destroy({
                where: {
                    uuid: uuid
                },
                transaction: tran
            });
            
            await tran.commit();
            return res.status(200).send({ status: 200, message: 'El usuario ha sido eliminado.', data: {} });
        } else {
            return res.status(404).send({ status: 404, message: 'No existe el usuario.', data: {} });
        }  
    }catch(error){
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al eliminar al usuario.', data: error.toString()});
    }
}

//Obtener Datos por Usuario
const getDataUser = async (req, res) => {
    const tran= await sequelize.transaction();    
    try{
        let uuid = req.params.id;
        let usuario = await usuarios.findOne({
            where: {
                uuid: uuid,
                activo: 1
            },
            raw: true,
            transaction: tran
        })
        if(usuario) {
            delete usuario.id;
            delete usuario.password;
            delete usuario.activo;
            await tran.commit();
            return res.status(200).send({ status: 200, message: '', data: usuario });
        } else {
            return res.status(404).send({ status: 404, message: 'No existe el usuario o se encuentra dado de baja.', data: {} });
        }
    }catch(error){
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al dar de baja al usuario.', data: error.toString()});
    }
}

async function encodeMd5(str) {
    return crypto.createHash('md5').update(str).digest('hex');
}

//Validación con expresiones regulares para contraseña Débil, Media o Fuerte
async function validarContraseña(str) {
    let rStr;
    let strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    let mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    let enoughRegex = new RegExp("(?=.{7,}).*", "g");

    if (false == enoughRegex.test(str)) {
        rStr = 'Contraseña débil ingrese más caracteres.'
    } else if (strongRegex.test(str)) {
        //Contraseña Fuerte
        rStr = true;
    } else if (mediumRegex.test(str)) {
        //Contraseña Media
        rStr = true;
    } else {
        rStr = 'Contraseña débil intente de nuevo.';
    }

    return rStr
}

module.exports = {
    loginUser,
    createUser,
    updateUser,
    disableUser,
    deleteUser,
    getDataUser
}