const moment = require("moment");
const { validationResult } = require('express-validator');
const { errResponse } = require('../middleware/HandleError/HandleError');

const {
    sequelize,
    usuarios,
    Appointmets
} = require('../db');

//Cita => Appointment

//Crear los horarios para las citas
const createAppointmentTime = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
        });
    }
    const tran = await sequelize.transaction();
    try {
        let { inicioDia, inicioComida, finComida, finDia, intervalos, fecha_cita } = req.body;

        if(inicioDia == ""){ inicioDia = "09:00"; }
        if(inicioComida == ""){ inicioComida = "13:00"; }
        if(finComida == ""){ finComida = "14:00"; }
        if(finDia == ""){ finDia = "19:00"; }
        if(intervalos <= 0 || intervalos > 60){ intervalos = 30; }
        if(fecha_cita == ""){ fecha_cita = moment().format('YYYY-MM-DD'); }

        let siguienteHora = "";
        siguienteHora = moment(inicioDia,"HH:mm").format("HH:mm");
        let allCitas = []
        for(let i = 0; siguienteHora < moment(finDia,"HH:mm").format("HH:mm"); i++) {
            
            if(!(siguienteHora >= moment(inicioComida,"HH:mm").format("HH:mm") 
            && siguienteHora < moment(finComida,"HH:mm").format("HH:mm"))){            
                let citasObj = {
                    fecha_cita: fecha_cita,
                    hora_cita: siguienteHora
                }
                allCitas.push(citasObj)
            }
            siguienteHora = moment(siguienteHora,"HH:mm").add(intervalos,'minutes').format("HH:mm");
        }

        let citas = await Appointmets.findOne({
            where: {
               fecha_cita:fecha_cita
            },
            raw: true,
            transaction: tran
        });
        if(citas) {
            await Appointmets.destroy({
                where: {
                   fecha_cita:fecha_cita
                },
                raw: true,
                transaction: tran
            });
        }
        //Validar si ya hay registros con la fecha de la cita
        await Appointmets.bulkCreate(allCitas,{
            raw: true,
            transaction: tran
        });

        await tran.commit();
        return res.status(200).send({ status: 200, message: 'Horario de citas creado correctamente.', data: {} });
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al crear horarios de citas', data: error.toString()});
    }
}

//Obtener Listado de Todas las Citas
const getAllAppointments = async (req, res) => {
    const tran = await sequelize.transaction();
    const allAppointmets = await Appointmets.findAll({
        where: {estatus:1},
        attributes: { exclude: ['fecha_alta','uuid_usuario','estatus'] },
        raw: true,
        transaction: tran
    });
    console.log(allAppointmets.length);

    if(allAppointmets.length > 0){
        await tran.commit();
        return res.status(200).send({ status: 200, message: 'getAllAppointments', data: allAppointmets });
    } else {
        await tran.rollback();
        return res.status(404).send({ status: 404, message: 'Sin Citas Disponibles.', data: {} });
    }
}

//Obtener Datos de Cita
const getDataAppointment = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
        });
    }
    const tran = await sequelize.transaction();

    try {
        let uuid = req.params.id;
        const dataAppointmet = await Appointmets.findOne({
            where: {
                uuid: uuid
            },
            attributes: { exclude: ['fecha_alta','estatus'] },
            raw: true,
            transaction: tran
        });
    
        if(dataAppointmet){
            dataAppointmet.usuario = "";
            if(dataAppointmet.uuid_usuario) {
                const usuario = await usuarios.findOne({
                    where: {
                        uuid: dataAppointmet.uuid_usuario
                    }
                });
                delete dataAppointmet.uuid_usuario;
                if(usuario) {
                    await tran.commit();
                    const dataRes = {
                        id: dataAppointmet.id,
                        fecha_cita: dataAppointmet.fecha_cita,
                        hora_cita: dataAppointmet.hora_cita,
                        uuid: dataAppointmet.uuid,
                        usuario: usuario.nombre + ' ' + usuario.apPaterno + ' ' + usuario.apMaterno
                    };
                    return res.status(200).send({ status: 200, message: 'Datos encontrados.', data: dataRes });
                } else {
                    await tran.commit();
                    return res.status(200).send({ status: 200, message: 'Datos encontrados.', data: dataAppointmet });
                }
            } else {
                await tran.commit();
                return res.status(200).send({ status: 200, message: 'Datos encontrados.', data: dataAppointmet });
            }
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'No existe la Cita Buscada.', data: {} });
        }
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al consultar los datos.', data: error.toString()});
    }
}

//Obtener informacion de las citas por usuario
const getUserAppointments = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
        });
    }
    const tran = await sequelize.transaction();

    try {
        let userUuid = req.params.id;

        const usuario = await usuarios.findOne({
            where: {
                uuid: userUuid
            }
        });
       
        if(usuario) {
            const dataAppointmets = await Appointmets.findAll({
                where: {
                    uuid_usuario: userUuid
                },
                attributes: { exclude: ['fecha_alta','uuid_usuario'] },
                raw: true,
                transaction: tran
            });
            if(dataAppointmets && dataAppointmets.length > 0) {
                await tran.commit();
                return res.status(200).send({ status: 200, message: 'Citas encontradas con exito.', data: dataAppointmets });
            } else {
                await tran.rollback();
                return res.status(404).send({ status: 404, message: 'No existen citas asociadas al Usuario.', data: {} });
            }
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'No existe el Usuario.', data: {} });
        }
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al consultar los datos.', data: error.toString()});
    }
}

//Agendar Cita
const MakeAnAppointment = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
        });
    }
    const tran = await sequelize.transaction();

    try {
        let { uuidCita, uuidUsuario } = req.body;
    
        const dataAppointmet = await Appointmets.findOne({
            where: {
                uuid: uuidCita,
                estatus: 1
            },
            attributes: { exclude: ['fecha_alta','uuid_usuario','estatus'] },
            raw: true,
            transaction: tran
        });
    
        if(dataAppointmet){
            dataAppointmet.usuario = "";

            const usuario = await usuarios.findOne({
                where: {
                    uuid: uuidUsuario
                }
            });
            if(usuario) {
                let cambios = {
                    uuid_usuario:uuidUsuario,
                    estatus: 2
                }
                await Appointmets.update(cambios,{
                    returning: true,
                    where: { uuid: uuidCita },
                    transaction: tran
                });
                
                await tran.commit();
                const dataRes = {
                    id: dataAppointmet.id,
                    fecha_cita: dataAppointmet.fecha_cita,
                    hora_cita: dataAppointmet.hora_cita,
                    uuid: dataAppointmet.uuid,
                    usuario: usuario.nombre + ' ' + usuario.apPaterno + ' ' + usuario.apMaterno
                };
                return res.status(200).send({ status: 200, message: 'Cita registrada con exito.', data: dataRes });
            } else {
                await tran.rollback();
                return res.status(404).send({ status: 404, message: 'No existe el usuario.', data: dataAppointmet });
            }
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'No existe la Cita Buscada.', data: {} });
        }
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al registrar su cita.', data: error.toString()});
    }
}

//Cancelar Cita
const cancelAppointment = async (req, res) => {
    let err = await errResponse(validationResult(req), res, 'error');
    if (err !== null) {
        return res.status(422).send({
            status: 422,
            message: 'Error en los parámetros de entrada.',
            data:{}
        });
    }
    const tran = await sequelize.transaction();

    try {
        let { uuidCita, uuidUsuario } = req.body;
    
        const dataAppointmet = await Appointmets.findOne({
            where: {
                uuid: uuidCita,
                uuid_usuario: uuidUsuario
            },
            attributes: { exclude: ['fecha_alta','uuid_usuario','estatus'] },
            raw: true,
            transaction: tran
        });
    
        if(dataAppointmet){
            dataAppointmet.usuario = "";
                let cambios = {
                    uuid_usuario:"",
                    estatus: 1
                }
                await Appointmets.update(cambios,{
                    returning: true,
                    where: { uuid: uuidCita },
                    transaction: tran
                });
                
                await tran.commit();
                return res.status(200).send({ status: 200, message: 'Cita cancelada con exito.', data: {} });
        } else {
            await tran.rollback();
            return res.status(404).send({ status: 404, message: 'No existe la Cita Buscada.', data: {} });
        }
    } catch(error) {
        await tran.rollback();
        console.log(error);
        return res.status(500).send({ status: 500, message: 'Se generó un error al cancelar su cita.', data: error.toString()});
    }
}

module.exports = {
    createAppointmentTime,
    getAllAppointments,
    getDataAppointment,
    getUserAppointments,
    MakeAnAppointment,
    cancelAppointment
}