module.exports = {
    port: process.env.PORT || 3000,
    DEV: process.env.DEV || true,
    dbName: process.env.DBNAME || 'reserva',
    dbUser: process.env.DBUSER || 'reserva',
    dbPassword: process.env.DBPWD || 'reserva',
    dbHost: process.env.DBHOST || '127.0.0.1',
    dbDialect: process.env.DIALECT || 'mysql',
    token_key: process.env.SECRET_TOKEN || 'eyJlbWFpbCI6Ikg0ZFZTWGlVekdWRkkyQUI5VHROMlNONW1NUCtDM2lSOXJONldDN21UdHNIRGxtQjhzRG4zZnNuRlRJanE3a1FUL2lLN0paeE8vdG9uQUwyQXFXMkZnUEhQa1FOVEVaZkkwUnczblBGeUxKNUZzaU16V3lIMWlsUkoyUHJCdnVLNFN3azlZcm5ZRitmbTd3bDNWNzJ5RFpCaWtsZ3lEVkRraXpLM2I0TktFODVZNUs4V0VuT2dURzBES0hvNm0vcGpzc0ZFZkV6UkszcG1xWEw0S0Y4ZUx6bTIrbGVrSUJiTkFHc2lubTJhZFhCWk9YQ3lteGVUcUJ6UlR3ZWxweWNCaTl0S3hLalg5ZWc4SStnbFUxYzRHTWdoT0pNWXplSXVuMnhrMm9OZzczQnNCbzAyRTZabkh0aGZmZWJZUzZSSENKQjViQlRQaWRiaHQ3ajRya1YvUSIsImNvZGVWZXJzaW9uQXBwIjoxLCJpbWVpIjoiNWQ5YmU5MzRkMjI1MzVhNiIsIm5hbWVWZXJzaW9uQXBwIjoiMC4wLjAuMSIsImlhdCI6MTYxNTQzNzMxOSwiZXhwIjoxNjE1NDM3Mzc5fQ'
}