const moment = require("moment");

module.exports = (sequelize, type) => {
    return sequelize.define('tab_appversion', {
        fecha_alta: {
            type: type.DATEONLY,
            allowNull: false,
            defaultValue: moment().format('YYYY-MM-DD'),
        },
        id: {
            type: type.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        plataforma: {
            type: type.STRING(7),
            allowNull: false,
        },
        version: {
            type: type.STRING(20),
            allowNull: false,
            defaultValue: '0.0.0.0'
        },
        uuid: {
            type: type.UUID,
            allowNull: false,
            defaultValue: type.UUIDV1
        },
        activo: {
            type: type.INTEGER,
            allowNull: false,
            defaultValue: 1
        }
    }, 
    {
        timestamps: false
    });
};
