const moment = require("moment");

module.exports = (sequelize, type) => {
    return sequelize.define('tab_usuarios', {
    fecha_alta: {
        type: type.DATEONLY,
        allowNull: false,
        defaultValue: moment().format('YYYY-MM-DD'),
    },
    id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrementIdentity: true,
        autoIncrement: true,
        allowNull: false,
    },
    nombre: {
        type: type.STRING,
        allowNull: false,
    },
    apPaterno: {
        type: type.STRING,
        allowNull: false,
    },
    apMaterno: {
        type: type.STRING,
        allowNull: true,
        defaultValue: '',
    },
    email: {
        type: type.STRING,
        allowNull: false,
    },
    password: {
        type: type.STRING,
        allowNull: false,
    },
    uuid: {
        type: type.UUID,
        allowNull: false,
        defaultValue: type.UUIDV1,
    },
    activo: {
        type: type.INTEGER,
        allowNull: false,
        defaultValue: 1,
    },
  }, 
  {
      timestamps: false
  });
};