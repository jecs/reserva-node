const moment = require("moment");

module.exports = (sequelize, type) => {
    return sequelize.define('tab_citas', {
    fecha_alta: {
        type: type.DATEONLY,
        allowNull: false,
        defaultValue: moment().format('YYYY-MM-DD'),
    },
    id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrementIdentity: true,
        autoIncrement: true,
        allowNull: false,
    },
    fecha_cita: {
        type: type.DATEONLY,
        allowNull: false,
        defaultValue: moment().format('YYYY-MM-DD'),
    },
    hora_cita: {
        type: type.TEXT('tiny'),
        allowNull: false,
        defaultValue: "",
    },
    uuid_usuario: {
        type: type.TEXT('tiny'),
        allowNull: false,
        defaultValue: "",
    },
    uuid: {
        type: type.UUID,
        allowNull: false,
        defaultValue: type.UUIDV1,
    },
    estatus: {
        type: type.INTEGER,
        allowNull: false,
        defaultValue: 1,
        comment: "0 cancelado - 1 disponible - 2 ocupado - 3 atendido"
    },
  }, 
  {
      timestamps: false
  });
};