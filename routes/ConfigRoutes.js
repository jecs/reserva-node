const express = require('express');
const router = express.Router();

const { obtenerToken } = require('../controller/TokenController');
const { createToken } = require('../middleware/Validators/tokenValidator');


router.post('/getToken', createToken, obtenerToken);

module.exports = router;