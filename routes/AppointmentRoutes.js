const express = require('express');
const router = express.Router();

const { createAppointmentTime, getAllAppointments, 
        getDataAppointment, getUserAppointments,
        MakeAnAppointment, cancelAppointment 
    } = require('../controller/AppointmentController');
const { checkAppointmentTime, checkMakeAnAppointment,checkCancelAppointment } = require('../middleware/Validators/AppointmentValidator');

//Crear Horario de citas
router.post('/crearDiaReservaciones', checkAppointmentTime, createAppointmentTime);
//Obtener Todas las citas
router.get('/obtenerCitas', getAllAppointments);
//Obtener informacion de una cita
router.get('/obtenerInfoCita/:id', getDataAppointment);
//Obtener informacion de las citas por usuario
router.get('/obtenerCitasUsuario/:id', getUserAppointments);
//Agendar Cita
router.post('/agendarCita', checkMakeAnAppointment, MakeAnAppointment);
//Cancelar Cita
router.post('/cancelarCita', checkCancelAppointment, cancelAppointment);

module.exports = router;
