const express = require('express');
const router = express.Router();

const { loginUser, createUser,updateUser,disableUser,deleteUser,getDataUser } = require('../controller/UsersController');
const { checkLogin, checkAltaUsuario, actualizarUsuario } = require('../middleware/Validators/UsuarioValidator');

//Login USER
router.post('/login', checkLogin, loginUser);

//Alta (Creación)
router.post('/createUser', checkAltaUsuario, createUser);

//Actualización
router.patch('/updateUser/:id', actualizarUsuario, updateUser);

//Baja
router.patch('/disableUser/:id', disableUser);

//Eliminar Usuario
router.delete('/deleteUser/:id', deleteUser);

//Obtener Datos por Usuario
router.get('/getDataUser/:id', getDataUser);

module.exports = router;
