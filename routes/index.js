'use strict';
const indexRouter = require('./index');
const usersRouter = require('./UsersRoutes');
const configRouter = require('./ConfigRoutes');
const appointmentRouter = require('./AppointmentRoutes');


module.exports = {
    indexRouter,
    usersRouter,
    configRouter,
    appointmentRouter,
};
