const crypto = require("crypto");
const path = require("path");
const fs = require("fs");

const encryptStringWithRsaPublicKey = function(toEncrypt, relativeOrAbsolutePathToPublicKey) {
    try{
        let absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
        let publicKey = fs.readFileSync(absolutePath, "utf8");
        let buffer = Buffer.from(toEncrypt);
        let encrypted = crypto.publicEncrypt(publicKey, buffer);
        return encrypted.toString("base64");
    } catch(ex) {
        return "ecrypt error";
    }
};

const decryptStringWithRsaPrivateKey = function(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
    try{
        let absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
        let privateKey = fs.readFileSync(absolutePath, "utf8");
        let buffer = Buffer.from(toDecrypt, "base64");
        let decrypted = crypto.privateDecrypt(privateKey, buffer);
        return decrypted.toString("utf8");
    } catch(ex) {
        return "decoding error";
    }
    
};

module.exports = {
    encryptStringWithRsaPublicKey: encryptStringWithRsaPublicKey,
    decryptStringWithRsaPrivateKey: decryptStringWithRsaPrivateKey
}