const { check } = require('express-validator');
const errForm = 'Error en el formato de los parámetros de entrada.';
module.exports = {
    checkAppointmentTime: [
        check('inicioDia').isString(),
        check('inicioComida').isString(),
        check('finComida').isString(),
        check('finDia').isString(),
        check('intervalos').isInt(),
        check('fecha_cita').isString()
    ],
    checkMakeAnAppointment: [
        check('uuidCita').isString(),
        check('uuidUsuario').isString(),
    ],
    checkCancelAppointment: [
        check('uuidCita').isString(),
        check('uuidUsuario').isString(),
    ]
};
