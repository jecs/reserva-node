const { check } = require('express-validator');
const errForm = 'Error en el formato de los parámetros de entrada.';
module.exports = {
    checkLogin: [
        check('email').isString(),
        check('password').isString(),
    ],
    checkAltaUsuario: [
        check('nombre').isString(),
        check('paterno').isString(),
        check('materno').isString(),
        check('email').isString(),
        check('password').isString(),
    ],
    actualizarUsuario: [
        check('password').isString(),
    ]
};
