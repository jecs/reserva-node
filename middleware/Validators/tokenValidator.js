const { check } = require('express-validator');

const errForm = 'Error en los parámetros de entrada.';
//errForm puede ser cambiado por otro mensaje o asignar mensajes unicos a cada validación
//check('email').notEmpty().withMessage("El campo email esta vacio")
module.exports = {
    createToken: [
        check('email').isString(),
        check('email').notEmpty(),
        check('codeVersionApp').isInt(),
        check('codeVersionApp').notEmpty(),
        check('imei').isString(),
        check('imei').notEmpty(),
        check('nameVersionApp').isString(),
        check('nameVersionApp').notEmpty(),
    ]
};
