const config = require('../../config/config');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const generateToken = (data) => {
    try {
        //Eg: 60, "2 days", "10h", "7d". 
        //A numeric value is interpreted as a seconds count. 
        //If you use a string be sure you provide the time units (days, hours, etc), 
        //otherwise milliseconds unit is used by default ("120" is equal to "120ms").
        const token = jwt.sign(data, config.token_key, { algorithm: 'HS256', expiresIn: '1h' });
        return token;
    } catch (error) {
        return error;
    }
}
const decodedToken = (header) => {
    try {
        const decoded = jwt.verify(header.split(' ')[1], config.token_key);
        return decoded;
    } catch (error) {
        return null;
    }
}

const verifyToken = (req, res, next) => {
    try {
        if(req.header('Authorization').split(' ')[0] == 'Bearer') {
            const decoded = jwt.verify(req.header('Authorization').split(' ')[1], config.token_key);
            
            if(req.header('Device') == decoded.imei 
            && req.header('Device') != "" 
            && req.header('Device') != null) {
                if(decoded.exp <= moment().unix()) {
                    return res.status(401).send({ status: 401, message: 'Token expirado' });
                } else {
                    next();
                }
            } else {
                return res.status(403).send({ status: 403, message: 'Token incorrecto' });
            }
        } else {
            return res.status(403).send({ status: 403, message: 'Token incorrecto' });
        }
    } catch (error) {
        switch (error.name) {
            case 'JsonWebTokenError':
                return res.status(403).send({ status: 403, message: 'Token incorrecto' });
            case 'TokenExpiredError':
                return res.status(401).send({ status: 401, message: 'Token expirado' });
            default:
                return res.status(403).send({ status: 403, message: 'Ocurrio un error con el token' });
        }
    }
}

module.exports = {
    generateToken,
    decodedToken,
    verifyToken
}