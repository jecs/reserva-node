'use strict';
const Sequelize = require('sequelize');
const config = require('./config/config');

const UsuariosModel = require('./models/Usuarios');
const AppVersionModel = require('./models/Appversion');
const AppointmentModel = require('./models/Appointments');

const sequelize = new Sequelize(config.dbName, config.dbUser, config.dbPassword,{
    logging: !config.DEV, //mostrar(true) u ocultar(false) log para ambiente desarrollo
    host: config.dbHost,
    dialect: config.dbDialect
});

const usuarios = UsuariosModel(sequelize, Sequelize);
const AppVersion = AppVersionModel(sequelize, Sequelize);
const Appointmets = AppointmentModel(sequelize, Sequelize);

sequelize.sync({force: false})
.then( () => {    
    //console.log("Base de datos sincronizada");
});

module.exports = {
    sequelize,
    usuarios,
    AppVersion,
    Appointmets,
};
